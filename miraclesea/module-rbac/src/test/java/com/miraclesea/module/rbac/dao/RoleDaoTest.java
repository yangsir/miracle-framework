package com.miraclesea.module.rbac.dao;

import static org.junit.Assert.assertNull;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import org.junit.Test;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringBeanByType;

import com.google.common.collect.Lists;
import com.miraclesea.module.rbac.entity.AssignedRolePermission;
import com.miraclesea.module.rbac.entity.DeniedRolePermission;
import com.miraclesea.module.rbac.entity.Role;
import com.miraclesea.test.layer.DaoBaseTest;

@DataSet
public final class RoleDaoTest extends DaoBaseTest {
	
	@SpringBeanByType
	private RoleDao roleDao;
	
	@Test
	public void findByName() {
		assertReflectionEquals(roleDao.findByName("Super Admin"), createFristRole());
		assertReflectionEquals(roleDao.findByName("Admin"), createSecondRole());
		assertReflectionEquals(roleDao.findByName("User"), createThirdRole());
		assertNull(roleDao.findByName("Empty"));
	}
	
	@Test
	public void findByNameWithExceptedId() {
		assertReflectionEquals(roleDao.findByName("Super Admin", "4"), createFristRole());
		assertReflectionEquals(roleDao.findByName("Admin", "4"), createSecondRole());
		assertReflectionEquals(roleDao.findByName("User", "4"), createThirdRole());
		assertNull(roleDao.findByName("Empty", "4"));
		assertNull(roleDao.findByName("Super Admin", "1"));
		assertNull(roleDao.findByName("Admin", "2"));
		assertNull(roleDao.findByName("User", "3"));
	}
	
	private Role createFristRole() {
		Role result = new Role();
		result.setId("1");
		result.setName("Super Admin");
		result.setVersion(0L);
		AssignedRolePermission assignedRolePermission = new AssignedRolePermission();
		assignedRolePermission.setId("1");
		assignedRolePermission.setPermissionName("com.miraclesea.module.rbac.vo.RbacPermission#AssignRolePermission");
		assignedRolePermission.setVersion(0L);
		assignedRolePermission.setRole(result);
		result.setAssignedPermissions(Lists.newArrayList(assignedRolePermission));
		DeniedRolePermission deniedRolePermission = new DeniedRolePermission();
		deniedRolePermission.setId("2");
		deniedRolePermission.setPermissionName("com.miraclesea.module.rbac.vo.RbacPermission#DenyRolePermission");
		deniedRolePermission.setVersion(0L);
		deniedRolePermission.setRole(result);
		result.setDeniedPermissions(Lists.newArrayList(deniedRolePermission));
		return result;
	}
	
	private Role createSecondRole() {
		Role result = new Role();
		result.setId("2");
		result.setName("Admin");
		result.setVersion(0L);
		AssignedRolePermission assignedRolePermission = new AssignedRolePermission();
		assignedRolePermission.setId("3");
		assignedRolePermission.setPermissionName("com.miraclesea.module.rbac.vo.RbacPermission#AssignRolePermission");
		assignedRolePermission.setVersion(0L);
		assignedRolePermission.setRole(result);
		result.setAssignedPermissions(Lists.newArrayList(assignedRolePermission));
		return result;
	}
	
	private Role createThirdRole() {
		Role result = new Role();
		result.setId("3");
		result.setName("User");
		result.setVersion(0L);
		return result;
	}
}
