package com.miraclesea.component.log;

import org.aspectj.lang.ProceedingJoinPoint;

final class PerformanceTrace {
	
	private final long startNanoTime;
	private long finishedNanoTime;
	private final ProceedingJoinPoint pjp;
	
	PerformanceTrace(final ProceedingJoinPoint pjp) {
		startNanoTime = System.nanoTime();
		this.pjp = pjp;
	}
	
	String getPerformanceInfo() {
		mark();
		String performanceInfo = "Method signature: %s, Spend time is: %d nanos, about %d millis, about %d seconds.";
		return String.format(performanceInfo, pjp.getSignature().toLongString(), getSpendNanos(), getSpendMillis(), getSpendSeconds());
	}
	
	private void mark() {
		finishedNanoTime = System.nanoTime();
	}
	
	private long getSpendNanos() {
		return Math.abs(startNanoTime - finishedNanoTime);
	}
	
	private long getSpendMillis() {
		return getSpendNanos() / 1000 / 1000;
	}
	
	private long getSpendSeconds() {
		return getSpendMillis() / 1000;
	}
}
