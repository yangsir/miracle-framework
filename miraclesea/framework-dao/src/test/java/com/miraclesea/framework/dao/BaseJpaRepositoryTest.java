package com.miraclesea.framework.dao;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringBeanByType;

import com.miraclesea.framework.dao.exception.OptimisticLockingException;
import com.miraclesea.framework.dao.exception.PrimaryKeyNotFoundException;
import com.miraclesea.framework.entity.TestEntity;
import com.miraclesea.test.layer.DaoBaseTest;

@DataSet
public final class BaseJpaRepositoryTest extends DaoBaseTest {
	
	@SpringBeanByType
	private TestEntityDao testEntityDao;
	
	@Test
	public void save() {
		TestEntity actual = new TestEntity();
		actual.setName("test3");
		testEntityDao.save(actual);
		assertThat(testEntityDao.count(), is(3L));
		for (TestEntity expected : testEntityDao.findAll()) {
			if ("1".equals(expected.getId()) || "2".equals(expected.getId())) {
				continue;
			}
			assertReflectionEquals(expected, actual);
		}
		
	}
	
	@Test(expected = PrimaryKeyNotFoundException.class)
	@Transactional(TransactionMode.ROLLBACK)
	public void updateNotNullFailureForRecordNotFound() {
		TestEntity actual = new TestEntity();
		actual.setId("notExist");
		testEntityDao.updateNotNull(actual);
	}
	
	@Test(expected = OptimisticLockingException.class)
	@Transactional(TransactionMode.ROLLBACK)
	public void updateNotNullFailureForOptimisticLock() {
		TestEntity actual = new TestEntity();
		actual.setId("1");
		actual.setVersion(-1L);
		testEntityDao.updateNotNull(actual);
	}
	
	@Test
	public void updateNotNullSuccess() {
		TestEntity actual = new TestEntity();
		actual.setId("1");
		actual.setExtraText("extraText");
		testEntityDao.updateNotNull(actual);
		assertThat(testEntityDao.count(), is(2L));
		assertReflectionEquals(testEntityDao.findAll().get(0), buildTestEntity("1", "test1", "extraText", 1L));
	}
	
	@Test(expected = PrimaryKeyNotFoundException.class)
	@Transactional(TransactionMode.ROLLBACK)
	public void deleteFailureForDataNotFound() {
		testEntityDao.delete("NotExistId");
	}
	
	@Test
	@ExpectedDataSet
	public void deleteSuccess() {
		testEntityDao.delete("1");
	}
	
	@Test(expected = PrimaryKeyNotFoundException.class)
	@Transactional(TransactionMode.ROLLBACK)
	public void findOneFailureForDataNotFound() {
		testEntityDao.findOne("NotExistId");
	}
	
	@Test
	public void findOneSuccess() {
		assertReflectionEquals(testEntityDao.findOne("1"), buildTestEntity("1", "test1", null, 0L));
		assertReflectionEquals(testEntityDao.findOne("2"), buildTestEntity("2", "test2", null, 10L));
	}
	
	@Test
	public void findAll() {
		Page<TestEntity> testEntities = testEntityDao.findAll(new PageRequest(0, 10));
		assertThat(testEntities.getTotalElements(), is(2L));
		assertThat(testEntities.getTotalPages(), is(1));
		assertThat(testEntities.getContent().size(), is(2));
		assertReflectionEquals(testEntities.getContent().get(0), buildTestEntity("1", "test1", null, 0L));
		assertReflectionEquals(testEntities.getContent().get(1), buildTestEntity("2", "test2", null, 10L));
		
		testEntities = testEntityDao.findAll(new PageRequest(0, 1));
		assertThat(testEntities.getTotalElements(), is(2L));
		assertThat(testEntities.getTotalPages(), is(2));
		assertThat(testEntities.getContent().size(), is(1));
		assertReflectionEquals(testEntities.getContent().get(0), buildTestEntity("1", "test1", null, 0L));
		
		testEntities = testEntityDao.findAll(new PageRequest(1, 1));
		assertThat(testEntities.getTotalElements(), is(2L));
		assertThat(testEntities.getTotalPages(), is(2));
		assertThat(testEntities.getContent().size(), is(1));
		assertReflectionEquals(testEntities.getContent().get(0), buildTestEntity("2", "test2", null, 10L));
	}
	
	private TestEntity buildTestEntity(final String id, final String name, final String extraText, final long version) {
		TestEntity result = new TestEntity();
		result.setId(id);
		result.setName(name);
		result.setExtraText(extraText);
		result.setVersion(version);
		return result;
	}
}
